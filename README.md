CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 
 INTRODUCTION
 ------------
 
 The Mark as read module allows you to create regulations, terms and
 notify users about their creation and updates. Users may mark as read
 and accept those documents and then administrators can see
 who read those regulations.
 
 
  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/mark_as_read
 
  * To submit bug reports and feature suggestions, or track changes:
    https://www.drupal.org/project/issues/mark_as_read

REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://www.drupal.org/docs/8/core/modules/views/overview)
 * Block (https://www.drupal.org/docs/8/core/modules/block/overview
 * Field (https://www.drupal.org/docs/8/core/modules/field/overview)
 * User (https://www.drupal.org/docs/8/core/modules/user/overview)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------
 
 * Configure the user permissions in Administration » People » Permissions:

   - View published Regulation entities
     
     Users with this permission will see published regulations at
     regulation view. This permission is granted on install to 
     authenticated users.
     
   - View unpublished Regulation entities
   
     Users with this permission will be able to access unpublished regulations.
     
   - Administer Acceptance submission entities

     Users with this permission will be able to access administration form to
     configure acceptance submission entities.
     This is useful for administrators.

   - Administer Regulation entities

     Users with this permission will be able to access administration form to
     configure  regulation entities. This is useful for administrators.
   
   - Create new Acceptance submission entities
   
     Users with this permission will be able to access creation form of
     acceptance submission entities. This is risky because it can be used
     to lie about which regulations user read.
     
   - Create new Regulation entities
    
     Users with this permission will be able to access creation form of
     regulation entities. This is useful for admins and moderators.
     
   - Delete all revisions
     
     Users with this permission will be able to delete any revision of
     regulation entity. This is useful for moderators and admins. 
   
   - Revert all Regulation revisions
   
     Users with this permission will be able to revert to any previous
     revision of regulation entity. This is useful for moderators and 
     administrators.
   
   - View all Regulation revisions
   
     Users with this permission will be able to access list of revisions
     of regulation entity. This is useful for moderators and administrators.
   
   - Delete Acceptance submission entities
   
     Users with this permission will be able to delete acceptance
     submissions. This is very risky and should be given only to
     people who are cleaning up after deleted regulation entity.
   
   - View published Acceptance submission entities
   
     Users with this permissions will be able to see list of acceptance
     submission entities. Can be useful to administrators who want to see
     who read regulation.
   
   - Delete Regulation entities
   
     Users with this permission will be able to delete regulation
     entities. Can be useful to moderators and admins.
   
   - Edit Acceptance submission entities
   
     Users with this permission will be able to access edit form
     of acceptance submission entities. This is very risky setting
     and should be given only in special cases, because it may be used
     to lie about what document user read.
   
   - Edit Regulation entities
     
     Users with this permission will be able to access edit form
     of regulation entities. Can be useful to moderators and admins.
   
 * Customize the regulation settings in Structure » Regulation settings

   - Notification settings:
        ---------------
        - Send notification to roles : 
         
         Notifications will be sent to users with those roles.
         
        - Send to author: 
        
         Tells us if notification should be sent to author.
   - Mail message settings:
        --------------
        - Updated title :
         
         Title of email which will be sent when regulation is updated.
        
        - Updated message :
         
         Body of email which will be sent when regulation is updated.
        
        - Created title :
    
         Title of email which will be sent when regulation is created.
        
        - Created message :
        
         Body of email which will be sent when regulation is created.
 
 * Customize the regulation settings in Structure » Acceptance
 submission settings
 
   - Acceptance submission settings:
        -----------------
        - Title of accept checkbox :
        
         Title of checkbox which must be accepted when user want to
         mark document as read.
        
        - Message above accept checkbox :
        
         Message which will be shown above checkbox which must be
         accepted when user want to mark document as read. 
         
        - Message of submit button :
         
         Message which will be displayed inside submit button.
         

 * If you want to show users who don't have e-mail address 
 notification  you should add 'Documents waiting to be accepted'
 block to site.
   
TROUBLESHOOTING TODO
---------------

 * If the menu does not display, check the following:

   - Are the "Access administration menu" and "Use the administration pages
     and help" permissions enabled for the appropriate roles?

   - Does html.tpl.php of your theme output the $page_bottom variable?

FAQ TODO
---

Q: I enabled "Aggregate and compress CSS files", but admin_menu.css is still
   there. Is this normal?

A: Yes, this is the intended behavior. the administration menu module only loads
   its stylesheet as needed (i.e., on page requests by logged-on, administrative
   users).
   
MAINTAINERS
-----------

Current maintainers:
 * Adrian Gaborek - https://www.drupal.org/u/adrian-gaborek

This project has been sponsored by:
 * <b>Abventor</b> <br>
   Specialized in consulting and planning of Drupal powered sites, Abventor
   services include web development, team-leasing, drupal sites optimization,
   support and many others. Visit https://www.abventor.com for more information.
