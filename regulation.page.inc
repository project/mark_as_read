<?php

/**
 * @file
 * Contains regulation.page.inc.
 *
 * Page callback for Regulation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Regulation templates.
 *
 * Default template: regulation.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_regulation(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
