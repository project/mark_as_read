<?php

namespace Drupal\mark_as_read;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Acceptance submission entities.
 *
 * @ingroup regulation
 */
class AcceptanceSubmissionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Acceptance submission ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\mark_as_read\Entity\AcceptanceSubmission $entity */
    $row['id'] = Link::createFromRoute(
      $entity->id(),
      'entity.acceptance_submission.edit_form',
      ['acceptance_submission' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
