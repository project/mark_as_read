<?php

namespace Drupal\mark_as_read\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Documents waiting to be read' block.
 *
 * @Block(
 *  id = "documents_waiting_to_be_read_block",
 *  admin_label = @Translation("Documents waiting to be read"),
 * )
 */
class DocumentsWaitingToBeReadBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * Entity type manager getter.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager.
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Current user getter.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   Current user.
   */
  protected function getCurrentUser() {
    return $this->currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'title' => 'Documents waiting to be read',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'documents_waiting_to_be_read_block';

    $regulation_storage = $this->getEntityTypeManager()->getStorage('regulation');
    $regulations = $regulation_storage->loadByProperties(['status' => 1]);

    $regulation_ids = [];

    foreach ($regulations as $regulation) {
      $regulation_ids[] = [$regulation->id(), $regulation->getRevisionId()];
    }

    $submission_storage = $this->getEntityTypeManager()->getStorage('acceptance_submission');
    $submissions = $submission_storage->loadByProperties(['user_id' => $this->getCurrentUser()->id()]);

    $submitted_to_ids = [];

    foreach ($submissions as $submission) {
      $reference_id = reset($submission->getSubmittedTo()->referencedEntities())->id();
      $reference_vid = $submission->getSubmittedToVid()->value;

      $submitted_to_ids[] = [$reference_id, $reference_vid];
    }

    $not_accepted_ids = $this->arrayDifference($regulation_ids, $submitted_to_ids);

    if (count($not_accepted_ids) === 0) {
      $build['#markup'] = 'There are no regulation documents to accept.';
    }

    foreach ($not_accepted_ids as $not_accepted_id) {
      $link = $regulations[reset($not_accepted_id)]->toLink()->toRenderable();
      $link['#suffix'] = '<br>';
      $build['regulations'][] = $link;
    }

    $build['#cache']['max-age'] = 0;
    return $build;
  }

  /**
   * Gets difference between two arrays.
   *
   * @param array $regulation_ids
   *   An array with entries which are array with two values:
   *   Id and vid of regulation document.
   * @param array $submission_ids
   *   An array with entries which are array with two values:
   *   Id and vid of regulation document.
   *
   * @return array
   *   An array with entries which are array with two values:
   *   Id and vid of regulation document.
   *   It contains data about documents which were not accepted.
   */
  protected function arrayDifference(array $regulation_ids, array $submission_ids) {
    $new_entry_index = 0;
    $return_array = [];

    foreach ($regulation_ids as $regulation_id) {
      $return_array[$new_entry_index] = $regulation_id;

      foreach ($submission_ids as $submission_id) {
        if ($regulation_id == $submission_id) {
          unset($return_array[$new_entry_index]);
          break;
        }
      }

      $new_entry_index = count($return_array);
    }

    return $return_array;
  }

}
