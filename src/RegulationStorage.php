<?php

namespace Drupal\mark_as_read;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\mark_as_read\Entity\RegulationInterface;

/**
 * Defines the storage handler class for Regulation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Regulation entities.
 *
 * @ingroup regulation
 */
class RegulationStorage extends SqlContentEntityStorage implements RegulationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RegulationInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {regulation_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritDoc}
   */
  public function revisionIdsById(int $id) {
    return $this->database->query(
      'SELECT vid FROM {regulation_revision} WHERE id=:id ORDER BY vid',
      [':id' => $id]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {regulation_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(RegulationInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {regulation_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('regulation_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
