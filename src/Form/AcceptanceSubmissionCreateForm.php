<?php

namespace Drupal\mark_as_read\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mark_as_read\Entity\AcceptanceSubmission;
use Drupal\mark_as_read\Entity\RegulationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AcceptanceSubmissionCreateForm.
 *
 * @package Drupal\mark_as_read\Form
 */
class AcceptanceSubmissionCreateForm extends FormBase {

  /**
   * Checkbox title.
   *
   * @var string
   */
  protected $checkboxTitle;

  /**
   * Checkbox message.
   *
   * @var string
   */
  protected $checkboxMessage;

  /**
   * Submit button message.
   *
   * @var string
   */
  protected $submitMessage;

  /**
   * AcceptanceSubmissionCreateForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->checkboxTitle = $configFactory->get('mark_as_read.settings')->get('accept_checkbox_title');
    $this->checkboxMessage = $configFactory->get('mark_as_read.settings')->get('accept_checkbox_message');
    $this->submitMessage = $configFactory->get('mark_as_read.settings')->get('submit_acceptance_message');
  }

  /**
   * Creates instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container with services.
   *
   * @return \Drupal\Core\Form\FormBase|static
   *   Instance with injected services.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'acceptance_submission_create';
  }

  /**
   * Builds form render array.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\mark_as_read\Entity\RegulationInterface|null $submitted_to
   *   Document to which acceptance will be submitted.
   *
   * @return array
   *   Form extended with more fields.
   */
  public function buildForm(array $form, FormStateInterface $form_state, RegulationInterface $submitted_to = NULL) {
    $form_state->set('submitted_to', $submitted_to);

    $form['accepted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('@checkboxTitle', ['@checkboxTitle' => $this->checkboxTitle]),
      '#description_display' => 'before',
      '#description' => $this->t('@checkboxMessage', ['@checkboxMessage' => $this->checkboxMessage]),
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('@submitMessage', ['@submitMessage' => $this->submitMessage]),
    ];

    return $form;
  }

  /**
   * Submits form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submission = AcceptanceSubmission::create([
      'submitted_to' => $form_state->get('submitted_to')->id(),
      'submitted_to_vid' => $form_state->get('submitted_to')->getRevisionId(),
    ]);
    $submission->save();
  }

}
