<?php

namespace Drupal\mark_as_read\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RegulationSettingsForm.
 *
 * @ingroup regulation
 */
class RegulationSettingsForm extends ConfigFormBase {


  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Creates instance and takes care of dependency injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container with services.
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\Core\Form\FormBase|void
   *   Instance of this class.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Entity type manager getter.
   *
   * @return \Drupal\Core\Entity\EntityTypeManager
   *   Entity type manager.
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'regulation_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mark_as_read.settings'];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mark_as_read.settings')->set('send_to_roles', $form_state->getValue('send_to_roles'))->save();
    $this->config('mark_as_read.settings')->set('send_to_author', $form_state->getValue('send_to_author'))->save();
    $this->config('mark_as_read.settings')->set('updated_title', $form_state->getValue('updated_title'))->save();
    $this->config('mark_as_read.settings')->set('updated_message', $form_state->getValue('updated_message'))->save();
    $this->config('mark_as_read.settings')->set('created_title', $form_state->getValue('created_title'))->save();
    $this->config('mark_as_read.settings')->set('created_message', $form_state->getValue('created_message'))->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Builds form structure.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mark_as_read.settings');
    $role_objects = $this->getEntityTypeManager()->getStorage('user_role')->loadMultiple();

    $system_roles = array_combine(array_keys($role_objects), array_map(function (RoleInterface $role) {
      return $role->label();
    }, $role_objects));

    unset($system_roles['anonymous']);

    $form['notification_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Notification settings'),
    ];

    $form['notification_settings']['send_to_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send notifications to roles:'),
      '#default_value' => $config->get('send_to_roles'),
      '#options' => $system_roles,
      '#description' => $this->t('Email notifications will be sent to users who have these roles.'),
    ];

    $form['notification_settings']['send_to_author'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send to author.'),
      '#default_value' => $config->get('send_to_author'),
      '#description' => $this->t('Should email notifications be sent to regulation author?'),
    ];

    $form['mail_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail message settings'),
    ];

    $form['mail_settings']['updated_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated title'),
      '#description' => $this->t('This is a title of email which is sent when regulation is updated. If you want to get title of document use [regulation:title].'),
      '#default_value' => $config->get('updated_title'),
      '#size' => 60,
    ];

    $form['mail_settings']['updated_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Updated message'),
      '#description' => $this->t("This message is sent in email when regulation is updated. If you want to get title of document use [regulation:title]. If you want to get url to document use [regulation:url]"),
      '#default_value' => $config->get('updated_message'),
      '#size' => 60,
    ];

    $form['mail_settings']['created_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Created title'),
      '#description' => $this->t('This is a title of email which is sent when regulation is created. If you want to get title of document use [regulation:title].'),
      '#default_value' => $config->get('created_title'),
      '#size' => 60,
    ];

    $form['mail_settings']['created_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Created message'),
      '#description' => $this->t('This message is sent in email when regulation is created. If you want to get title of document use [regulation:title]. If you want to get url to document use [regulation:url]'),
      '#default_value' => $config->get('created_message'),
      '#size' => 60,
    ];

    return parent::buildForm($form, $form_state);
  }

}
