<?php

namespace Drupal\mark_as_read\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AcceptanceSubmissionSettingsForm.
 *
 * @ingroup regulation
 */
class AcceptanceSubmissionSettingsForm extends ConfigFormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Creates instance and takes care of dependency injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container with services.
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\Core\Form\FormBase|void
   *   Instance of this class.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * Entity type manager getter.
   *
   * @return \Drupal\Core\Entity\EntityTypeManager
   *   Entity type manager.
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'acceptancesubmission_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mark_as_read.settings'];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mark_as_read.settings')->set('show_to_roles', $form_state->getValue('show_to_roles'))->save();
    $this->config('mark_as_read.settings')->set('accept_checkbox_title', $form_state->getValue('accept_checkbox_title'))->save();
    $this->config('mark_as_read.settings')->set('accept_checkbox_message', $form_state->getValue('accept_checkbox_message'))->save();
    $this->config('mark_as_read.settings')->set('submit_acceptance_message', $form_state->getValue('submit_acceptance_message'))->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Acceptance submission entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mark_as_read.settings');

    $role_objects = $this->getEntityTypeManager()->getStorage('user_role')->loadMultiple();
    $system_roles = array_combine(array_keys($role_objects), array_map(function (RoleInterface $role) {
      return $role->label();
    }, $role_objects));

    unset($system_roles['anonymous']);

    $form['acceptance_form_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mark as read form configuration'),
    ];

    $form['acceptance_form_settings']['show_to_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Show form to roles:'),
      '#default_value' => $config->get('show_to_roles'),
      '#options' => $system_roles,
      '#description' => $this->t('Acceptance form will be shown to users who have these one of roles.'),
    ];

    $form['acceptance_form_settings']['accept_checkbox_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title of accept checkbox.'),
      '#description' => $this->t('Title of checkbox which must be accepted when user want to mark document as read.'),
      '#default_value' => $config->get('accept_checkbox_title'),
      '#size' => 60,
    ];

    $form['acceptance_form_settings']['accept_checkbox_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message above accept checkbox'),
      '#description' => $this->t('Message which will be shown above checkbox which must be accepted when user want to mark document as read.'),
      '#default_value' => $config->get('accept_checkbox_message'),
      '#size' => 60,
    ];

    $form['acceptance_form_settings']['submit_acceptance_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message of submit button'),
      '#default_value' => $config->get('submit_acceptance_message'),
      '#size' => 60,
    ];
    return parent::buildForm($form, $form_state);
  }

}
