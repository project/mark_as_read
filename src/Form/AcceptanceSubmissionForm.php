<?php

namespace Drupal\mark_as_read\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Acceptance submission edit forms.
 *
 * @ingroup regulation
 */
class AcceptanceSubmissionForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $revision_id = $form_state->getValue('submitted_to_vid')[0]['value'];
    $regulation_id = $form_state->getValue('submitted_to')[0]['target_id'] ?? -1;
    $vids = $this->entityTypeManager->getStorage('regulation')->revisionIdsById($regulation_id);

    if (!in_array($revision_id, $vids)) {
      $form_state->setErrorByName('submitted_to_vid', 'Regulation with this revision id does not exist.');
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Acceptance submission.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Acceptance submission.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.acceptance_submission.canonical', ['acceptance_submission' => $entity->id()]);
  }

}
