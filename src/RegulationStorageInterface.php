<?php

namespace Drupal\mark_as_read;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\mark_as_read\Entity\RegulationInterface;

/**
 * Defines the storage handler class for Regulation entities.
 *
 * This extends the base storage class, adding required special handling for
 * Regulation entities.
 *
 * @ingroup regulation
 */
interface RegulationStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Regulation revision IDs for a specific Regulation.
   *
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   The Regulation entity.
   *
   * @return int[]
   *   Regulation revision IDs (in ascending order).
   */
  public function revisionIds(RegulationInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Regulation author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Regulation revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   The Regulation entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(RegulationInterface $entity);

  /**
   * Unsets the language for all Regulation with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
