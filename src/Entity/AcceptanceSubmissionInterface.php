<?php

namespace Drupal\mark_as_read\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Acceptance submission entities.
 *
 * @ingroup regulation
 */
interface AcceptanceSubmissionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Acceptance submission submitted to.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   Regulation entity.
   */
  public function getSubmittedTo();

  /**
   * Sets the Acceptance submission entity submitted to.
   *
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   Entity to which submission was made.
   *
   * @return \Drupal\mark_as_read\Entity\AcceptanceSubmissionInterface
   *   The called Acceptance submission entity.
   */
  public function setSubmittedTo(RegulationInterface $entity);

  /**
   * Gets the Acceptance submission submitted to.
   *
   * @return int
   *   Id of revision.
   */
  public function getSubmittedToVid();

  /**
   * Sets the Acceptance submission entity submitted to vid.
   *
   * @param int $vid
   *   Revision to which submission was made.
   *
   * @return \Drupal\mark_as_read\Entity\AcceptanceSubmissionInterface
   *   The called Acceptance submission entity.
   */
  public function setSubmittedToVid(int $vid);

  /**
   * Gets the Acceptance submission creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Acceptance submission.
   */
  public function getCreatedTime();

  /**
   * Sets the Acceptance submission creation timestamp.
   *
   * @param int $timestamp
   *   The Acceptance submission creation timestamp.
   *
   * @return \Drupal\mark_as_read\Entity\AcceptanceSubmissionInterface
   *   The called Acceptance submission entity.
   */
  public function setCreatedTime($timestamp);

}
