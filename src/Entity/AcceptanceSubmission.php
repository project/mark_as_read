<?php

namespace Drupal\mark_as_read\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Acceptance submission entity.
 *
 * @ingroup regulation
 *
 * @ContentEntityType(
 *   id = "acceptance_submission",
 *   label = @Translation("Acceptance submission"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mark_as_read\AcceptanceSubmissionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\mark_as_read\Form\AcceptanceSubmissionForm",
 *       "add" = "Drupal\mark_as_read\Form\AcceptanceSubmissionForm",
 *       "edit" = "Drupal\mark_as_read\Form\AcceptanceSubmissionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\mark_as_read\AcceptanceSubmissionHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\mark_as_read\AcceptanceSubmissionAccessControlHandler",
 *   },
 *   base_table = "acceptance_submission",
 *   data_table = "acceptance_submission_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer acceptance submission entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/acceptance_submission/{acceptance_submission}",
 *     "add-form" = "/admin/structure/acceptance_submission/add",
 *     "edit-form" = "/admin/structure/acceptance_submission/{acceptance_submission}/edit",
 *     "delete-form" = "/admin/structure/acceptance_submission/{acceptance_submission}/delete",
 *     "collection" = "/admin/structure/acceptance_submission",
 *   },
 *   field_ui_base_route = "acceptance_submission.settings"
 * )
 */
class AcceptanceSubmission extends ContentEntityBase implements AcceptanceSubmissionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmittedTo() {
    return $this->get('submitted_to');
  }

  /**
   * {@inheritdoc}
   */
  public function setSubmittedTo(RegulationInterface $entity) {
    $this->set('submitted_to', $entity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmittedToVid() {
    return $this->get('submitted_to_vid');
  }

  /**
   * {@inheritdoc}
   */
  public function setSubmittedToVid(int $vid) {
    $this->set('submitted_to_vid', $vid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Acceptance submission entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['submitted_to'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Submitted to.'))
      ->setDescription(t('Regulation to which this submission applies'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'regulation')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'regulation',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['submitted_to_vid'] = BaseFieldDefinition::create('integer')
      ->setLabel('Submitted to revision.')
      ->setDescription('Revision of regulation to which this submission applies')
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
