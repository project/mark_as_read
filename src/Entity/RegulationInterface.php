<?php

namespace Drupal\mark_as_read\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Regulation entities.
 *
 * @ingroup regulation
 */
interface RegulationInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the Regulation name.
   *
   * @return string
   *   Name of the Regulation.
   */
  public function getName();

  /**
   * Sets the Regulation name.
   *
   * @param string $name
   *   The Regulation name.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   The called Regulation entity.
   */
  public function setName($name);

  /**
   * Gets the Regulation body.
   *
   * @return string
   *   Regulation body.
   */
  public function getBody();

  /**
   * Sets the Regulation body.
   *
   * @param string $body
   *   String which contains body of regulation document.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   The called Regulation entity.
   */
  public function setBody($body);

  /**
   * Gets the Regulation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Regulation.
   */
  public function getCreatedTime();

  /**
   * Sets the Regulation creation timestamp.
   *
   * @param int $timestamp
   *   The Regulation creation timestamp.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   The called Regulation entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Regulation revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Regulation revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   The called Regulation entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Regulation revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Regulation revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\mark_as_read\Entity\RegulationInterface
   *   The called Regulation entity.
   */
  public function setRevisionUserId($uid);

}
