<?php

namespace Drupal\mark_as_read;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Acceptance submission entity.
 *
 * @see \Drupal\mark_as_read\Entity\AcceptanceSubmission.
 */
class AcceptanceSubmissionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mark_as_read\Entity\AcceptanceSubmissionInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view acceptance submission entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit acceptance submission entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete acceptance submission entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add acceptance submission entities');
  }

}
