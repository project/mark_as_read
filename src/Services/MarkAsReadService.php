<?php

namespace Drupal\mark_as_read\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\mark_as_read\Entity\RegulationInterface;

/**
 * Provides a way to display acceptance form on regulations.
 */
class MarkAsReadService {

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;


  /**
   * Submission storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $submissionStorage;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;


  /**
   * Form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  private $formBuilder;

  /**
   * Array of roles to which form will be shown.
   *
   * @var array
   */
  private $showToRoles;

  /**
   * MarkAsRead Service constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   Form builder service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, AccountInterface $currentUser, EntityTypeManagerInterface $entityTypeManager, FormBuilderInterface $formBuilder) {
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
    $this->showToRoles = $configFactory->get('mark_as_read.settings')->get('show_to_roles');
  }

  /**
   * Submission storage getter.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Submission storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSubmissionStorage() {
    if ($this->submissionStorage === NULL) {
      $this->submissionStorage = $this->entityTypeManager->getStorage('acceptance_submission');
    }
    return $this->submissionStorage;
  }

  /**
   * Current user getter.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   Current user.
   */
  protected function getCurrentUser() {
    return $this->currentUser;
  }

  /**
   * Entity type manager getter.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager.
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Form builder getter.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   Form builder.
   */
  protected function getFormBuilder() {
    return $this->formBuilder;
  }

  /**
   * Show to roles getter.
   *
   * @return array
   *   An array which contain roles to which submission form will be shown.
   */
  protected function getShowToRoles() {
    return $this->showToRoles;
  }

  /**
   * Checks if user accepted regulation provided in parameter.
   *
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   Regulation whose acceptance submissions will be checked.
   *
   * @return bool
   *   Determines if user has already accepted regulation.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function userAcceptedRegulation(RegulationInterface $entity) {
    if ($this->getSubmissionStorage()->hasData()) {
      $submissions = $this->getSubmissionStorage()->loadByProperties(
          [
            'user_id' => $this->getCurrentUser()->id(),
            'submitted_to' => $entity->id(),
            'submitted_to_vid' => $entity->getRevisionId(),
          ]);

      return !empty($submissions);
    }
    return FALSE;
  }

  /**
   * Check if user have role which will allow user to access form.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User whose roles will be checked.
   *
   * @return bool
   *   Determines if user have role which will allow user to access form.
   */
  protected function hasUserAccessToForm(AccountInterface $user) {
    if (empty($this->getShowToRoles())) {
      return FALSE;
    }

    return !empty(array_intersect($user->getRoles(), $this->getShowToRoles()));
  }

  /**
   * Displays acceptance submission form.
   *
   * @param array $build
   *   Render array.
   * @param string $view_mode
   *   View mode.
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   Regulation entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function displayAcceptanceForm(array &$build, string $view_mode, RegulationInterface $entity) {
    if ($view_mode === 'full' && $this->hasUserAccessToForm($this->getCurrentUser()) && $entity->isPublished()) {
      if (!$this->userAcceptedRegulation($entity)) {
        $form = $this->getFormBuilder()->getForm('Drupal\mark_as_read\Form\AcceptanceSubmissionCreateForm', $entity);
        $build['document_form'] = $form;
      }
      // Disable cache so that if submission is deleted form shows again.
      $build['#cache']['max-age'] = 0;
    }
  }

}
