<?php

namespace Drupal\mark_as_read\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\mark_as_read\Entity\RegulationInterface;

/**
 * Manages and provides a way to send email notifications about regulations.
 */
class MailService {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Submission storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $submissionStorage;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * Bool which tells us whether notification should be sent to author.
   *
   * @var bool
   */
  private $sendToAuthor;

  /**
   * Array of roles which will get notifications.
   *
   * @var array
   */
  private $sendToRoles;

  /**
   * Constructs a MailService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, MailManagerInterface $mail_manager) {
    $this->sendToRoles = $config_factory->get('mark_as_read.settings')->get('send_to_roles');
    $this->sendToAuthor = $config_factory->get('mark_as_read.settings')->get('send_to_author');
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
  }

  /**
   * Mail manager getter.
   *
   * @return \Drupal\Core\Mail\MailManagerInterface
   *   Mail manager.
   */
  protected function getMailManager() {
    return $this->mailManager;
  }

  /**
   * Entity type manager getter.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager.
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * Send to roles getter.
   *
   * @return array
   *   Array of roles which will get notifications.
   */
  protected function getSendToRoles() {
    return $this->sendToRoles;
  }

  /**
   * Send to author getter.
   *
   * @return bool
   *   Bool which tells us if notification should be sent to author of
   *   regulation document.
   */
  protected function getSendToAuthor() {
    return $this->sendToAuthor;
  }

  /**
   * Submission storage getter.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Submission storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getSubmissionStorage() {
    if ($this->submissionStorage === NULL) {
      $this->submissionStorage = $this->entityTypeManager->getStorage('acceptance_submission');
    }
    return $this->submissionStorage;
  }

  /**
   * Checks if user is author of regulation.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User which is potential author of regulation entity.
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   Regulation entity.
   *
   * @return bool
   *   Determines if user is author of regulation.
   */
  protected function isUserAuthorOfRegulation(AccountInterface $user, RegulationInterface $entity) {
    return $entity->getRevisionUser()->id() === $user->id();
  }

  /**
   * Checks if regulation is read and accepted by user.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User whose acceptance submissions will be loaded.
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $entity
   *   Regulation whose acceptance submissions will be checked.
   *
   * @return bool
   *   Determines if user has already read and accepted regulation.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function isRegulationAcceptedByUser(AccountInterface $user, RegulationInterface $entity) {
    if ($this->getSubmissionStorage()->hasData()) {
      $submissions = $this->getSubmissionStorage()->loadByProperties(
        [
          'user_id' => $user->id(),
          'submitted_to' => $entity->id(),
          'submitted_to_vid' => $entity->getRevisionId(),
        ]);
      return !empty($submissions);
    }
    return FALSE;
  }

  /**
   * Check if user has role which allows to get notifications.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User whose roles will be checked.
   *
   * @return bool
   *   Determines if user has ability to get notifications.
   */
  protected function isUserAbleToGetNotifications(AccountInterface $user) {
    if (empty($this->getSendToRoles())) {
      return FALSE;
    }

    return !empty(array_intersect($user->getRoles(), $this->getSendToRoles()));
  }

  /**
   * Send email notification to users.
   *
   * @param \Drupal\mark_as_read\Entity\RegulationInterface $regulation
   *   Regulation entity.
   * @param string $mail_key
   *   Mail key which contains info about whether regulation was
   *   created or updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function sendEmailNotification(RegulationInterface $regulation, string $mail_key) {
    // If regulation is not published don't try to send emails.
    if (!$regulation->isPublished()) {
      return;
    }

    // If there was only change without making revision and changing published
    // status don't send emails.
    if (isset($regulation->original) && $regulation->original->isPublished() === $regulation->isPublished() && $regulation->original->getRevisionId() == $regulation->getRevisionId()) {
      return;
    }

    /** @var \Drupal\Core\Session\AccountInterface[] $users */
    $users = $this->getEntityTypeManager()->getStorage('user')->loadByProperties(['status' => 1]);
    $module = 'mark_as_read';
    $send = TRUE;
    $params['regulation_title'] = $regulation->label();
    $params['regulation_url'] = $regulation->toLink()->toString();

    foreach ($users as $user) {
      // If user don't have role which will allow user to get notifications
      // don't send them.
      if (!$this->isUserAbleToGetNotifications($user)) {
        continue;
      }

      // If regulation is already read don't send notification.
      if ($this->isRegulationAcceptedByUser($user, $regulation)) {
        continue;
      }

      // If this bool is false don't send notification to author.
      if (!$this->getSendToAuthor() && $this->isUserAuthorOfRegulation($user, $regulation)) {
        continue;
      }

      $langcode = $user->getPreferredLangcode();
      $to = $user->getEmail();

      // If user doesn't have email don't send notification.
      if ($to == '') {
        continue;
      }

      $this->getMailManager()->mail($module, $mail_key, $to, $langcode, $params, NULL, $send);
    }
  }

}
