<?php

namespace Drupal\mark_as_read;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Regulation entity.
 *
 * @see \Drupal\mark_as_read\Entity\Regulation.
 */
class RegulationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mark_as_read\Entity\RegulationInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished regulation entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published regulation entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit regulation entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete regulation entities');
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add regulation entities');
  }

}
