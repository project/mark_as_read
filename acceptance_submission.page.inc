<?php

/**
 * @file
 * Contains acceptance_submission.page.inc.
 *
 * Page callback for Acceptance submission entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Acceptance submission templates.
 *
 * Default template: acceptance_submission.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_acceptance_submission(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
